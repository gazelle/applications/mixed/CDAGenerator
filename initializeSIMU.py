## author : abderrazek boufahja

import os
import dircache
import shutil

repiss = '/Users/aboufahj/Documents/workspace/CDAGenerator'

SimName = 'CDAGenerator'

def removesvn(repis):
    list_output = dircache.listdir(repis)
    for outp in list_output:
        if outp==".svn":
            shutil.rmtree(repis + '/' + outp);
        else:
            if os.path.isdir(repis + '/' + outp):
                removesvn(repis + '/' + outp)

               
                
def renameSimulator(repis, SimName):
    list_output = dircache.listdir(repis)
    for outp in list_output:
        if (outp.index('EVSClient')>(-1)):
            outptwo = outp.replace('EVSClient', SimName)
            os.rename(outp, outptwo)
        if os.path.isfile(outp):
            fich = open(repis + '/' +  outp);
            fill = fich.read()
            fill = fill.replace('EVSClient', SimName)
            fich.close()
            fich = open(repis + '/' +  outp)
            fich.write(fill)
            fich.close()
        
def renameFileSim(repis, SimName):
    list_output = dircache.listdir(repis)
    for outp in list_output:
        if (outp.count('EVSClient')>0):
            outptwo = outp.replace('EVSClient', SimName)
            os.rename(repis + '/' + outp, repis + '/' + outptwo)
            if os.path.isdir(repis + '/' + outptwo):
                renameFileSim(repis + '/' + outptwo, SimName)
        else :
            if os.path.isdir(repis + '/' + outp):
                renameFileSim(repis + '/' + outp, SimName)
                

def replaceSim(repis, SimName):
    list_output = dircache.listdir(repis)
    for outp in list_output:
        if os.path.isfile(repis + '/' + outp):
            if outp!='initializeSIMU.py':
                fich = open(repis + '/' +  outp,'r')
                fill = fich.read()
                fill = fill.replace('EVSClient', SimName)
                fich.close()
                fich2 = open(repis + '/' +  outp,'w')
                fich2.write(fill)
                fich2.close()
        else:
            if os.path.isdir(repis + '/' + outp):
                replaceSim(repis + '/' + outp, SimName)
                
            
removesvn(repiss)
renameFileSim(repiss, SimName)
replaceSim(repiss, SimName)
