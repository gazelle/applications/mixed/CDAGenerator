package net.ihe.gazelle.cdagen.bbr.model;

public enum JavaVersionEnum {

    JAVA_7("1.7"),
    JAVA_11("11");

    private final String label;

    JavaVersionEnum(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
