package net.ihe.gazelle.cdagen.bbr.action;

import java.util.Map;

import javax.persistence.EntityManager;

import net.ihe.gazelle.cdagen.bbr.model.TCDescriber;
import net.ihe.gazelle.cdagen.bbr.model.TCDescriberQuery;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;

/**
 * 
 * @author Abderrazek Boufahja
 * 
 */
public class TCDescriberFilter extends Filter<TCDescriber> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TCDescriberFilter(Map<String, String> requestParameterMap) {
		super(getHQLCriterions(), requestParameterMap);
	}

	private static HQLCriterionsForFilter<TCDescriber> getHQLCriterions() {

		TCDescriberQuery query = new TCDescriberQuery(EntityManagerService.provideEntityManager());
		return query.getHQLCriterionsForFilter();
	}

	@Override
	public EntityManager getEntityManager() {
		return EntityManagerService.provideEntityManager();
	}

}
