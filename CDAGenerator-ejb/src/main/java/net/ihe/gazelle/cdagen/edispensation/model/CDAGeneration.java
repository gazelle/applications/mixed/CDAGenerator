package net.ihe.gazelle.cdagen.edispensation.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.cdagen.model.Organization;
import net.ihe.gazelle.cdagen.model.Patient;

@Entity
@Name("cdaGeneration")
@Table(name="cda_generation", schema="public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cda_generation_sequence", sequenceName = "cda_generation_id_seq", allocationSize=1)
public class CDAGeneration implements Serializable {
    

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @NotNull
    @Column(name="id", nullable = false, unique = true)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cda_generation_sequence")
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "prescription_id", unique = false, nullable = true)
    private CDAFile prescription;
    
    @ManyToOne
    @JoinColumn(name = "dispensation_id", unique = false, nullable = true)
    private CDAFile dispensation;
    
    @Column(name="date_creation")
    private Date dateCreation;
    
    @ManyToOne
    @JoinColumn(name = "author_id", unique = false, nullable = true)
    private Patient author;
    
    @ManyToOne
    @JoinColumn(name = "organization_id", unique = false, nullable = true)
    private Organization organization;
    
    @JoinColumn(name = "creator", unique = false, nullable = true)
    private String creator;
    
    @JoinColumn(name = "affinity_domain_id", unique = false, nullable = true)
    private AffinityDomain affinityDomain;

    

    public CDAGeneration(){}
    
    public CDAGeneration(CDAFile prescription,
            CDAFile dispensation, Date dateCreation, Patient author, Organization org, AffinityDomain ad) {
        super();
        this.prescription = prescription;
        this.dispensation = dispensation;
        this.dateCreation = dateCreation;
        this.author = author;
        this.organization = org;
        this.affinityDomain = ad;
    }
    
    



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AffinityDomain getAffinityDomain() {
        return affinityDomain;
    }

    public void setAffinityDomain(AffinityDomain affinityDomain) {
        this.affinityDomain = affinityDomain;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public CDAFile getPrescription() {
        return prescription;
    }

    public void setPrescription(CDAFile prescription) {
        this.prescription = prescription;
    }

    public CDAFile getDispensation() {
        return dispensation;
    }

    public void setDispensation(CDAFile dispensation) {
        this.dispensation = dispensation;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Patient getAuthor() {
        return author;
    }

    public void setAuthor(Patient author) {
        this.author = author;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((affinityDomain == null) ? 0 : affinityDomain.hashCode());
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result
                + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result
                + ((dispensation == null) ? 0 : dispensation.hashCode());
        result = prime * result
                + ((organization == null) ? 0 : organization.hashCode());
        result = prime * result
                + ((prescription == null) ? 0 : prescription.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        CDAGeneration other = (CDAGeneration) obj;
        if (affinityDomain != other.affinityDomain){
            return false;
        }
        if (author == null) {
            if (other.author != null){
                return false;
            }
        } else if (!author.equals(other.author))
            return false;
        if (creator == null) {
            if (other.creator != null)
                return false;
        } else if (!creator.equals(other.creator)){
            return false;
        }
        if (dateCreation == null) {
            if (other.dateCreation != null){
                return false;
            }
        } else if (!dateCreation.equals(other.dateCreation)){
            return false;
        }
        if (dispensation == null) {
            if (other.dispensation != null)
                return false;
        } else if (!dispensation.equals(other.dispensation))
            return false;
        if (organization == null) {
            if (other.organization != null){
                return false;
            }
        } else if (!organization.equals(other.organization)){
            return false;
        }
        if (prescription == null) {
            if (other.prescription != null){
                return false;
            }
        } else if (!prescription.equals(other.prescription)){
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "CDAGeneration [id=" + id + ", prescription=" + prescription
                + ", dispensation=" + dispensation + ", dateCreation="
                + dateCreation + ", author=" + author + ", organization="
                + organization + ", creator=" + creator + ", affinityDomain="
                + affinityDomain + "]";
    }
    
    public String stringCreator(){
        if (this.creator != null){
            return this.creator;
        }
        return "UNKNOWN";
    }

}
