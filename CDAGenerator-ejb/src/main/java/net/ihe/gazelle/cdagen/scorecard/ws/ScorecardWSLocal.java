package net.ihe.gazelle.cdagen.scorecard.ws;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * <b>Class Description : </b>ScorecardWSLocal<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 06/12/16
 * @class ScorecardWSLocal
 * @package net.ihe.gazelle.cda.scorecard.ws
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@Local
@Path("/scorecard")
@Api("/scorecard")
public interface ScorecardWSLocal {

    @GET
    @Path("/hello")
    @Produces("text/plain")
    @ApiOperation(value = "hello operation", responseClass = "javax.ws.rs.core.Response")
    Response hello(@ApiParam(value = "Hello to scorecard")  @Context HttpServletRequest req);

    @POST
    @Path("/compute")
    @Consumes("text/plain")
    @ApiOperation(value = "compute the scorecard without returning the result, asynchronous operation", responseClass = "javax.ws.rs.core.Response")
    Response computeScorecard(@ApiParam(value = "DetailedResult of validation in B64") String b64DetailedResult, 
    		@ApiParam(value = "OID of the DetailedResult validation comping from EVSClient") @QueryParam("oid") String oid);

    @GET
    @Path("/get")
    @Produces("application/xml")
    @ApiOperation(value = "get the result of the scorecard, for asynchronous usage", responseClass = "javax.ws.rs.core.Response")
    Response getScorecard(@ApiParam(value = "Reload the result of the scoring using the OID of the scorecard") @QueryParam("oid") String oid);

    @POST
    @Path("/computeAndReturnImmediately")
    @Consumes("text/plain")
    @ApiOperation(value = "compute the scorecard and return the result to the user.", responseClass = "javax.ws.rs.core.Response")
    Response computeAndReturnScorecard(@ApiParam(value = "DetailedResult of validation in B64") String b64DetailedResult, 
    		@ApiParam(value = "OID of the DetailedResult validation comping from EVSClient") @QueryParam("oid") String oid);
}
