package net.ihe.gazelle.cdagen.validator.ws;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.cdagen.bbr.model.JavaVersionEnum;
import net.ihe.gazelle.utils.FileReadWrite;
import org.jboss.seam.annotations.Name;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.bbr.action.BBRUtil;
import net.ihe.gazelle.cdagen.bbr.action.ProcessBBR;
import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;
import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepositoryQuery;
import net.ihe.gazelle.cdagen.bbr.model.ValidatorDescriber;
import net.ihe.gazelle.cdagen.simulator.ws.AbstractModelBasedValidation;
import net.ihe.gazelle.validation.DetailedResult;

/**
 * Webservice for the validation of CDA documents defined by CDA validation tool.
 * targetNamespace : http://ws.mb.validator.gazelle.ihe.net
 * webservice : ModelBasedValidationWS
 * webservice's service name : ModelBasedValidationWSService
 * webservice's port name : ModelBasedValidationWSPort
 *
 * @author Abderrazek Boufahja
 */
@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS",
        serviceName = "ModelBasedValidationWSService",
        portName = "ModelBasedValidationWSPort",
        targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class CDAValidatorWS extends AbstractModelBasedValidation implements Serializable {

    private static org.slf4j.Logger log = LoggerFactory.getLogger(CDAValidatorWS.class);

    public static final int BUFFER_SIZE = 8192;

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * A method containing a description of the webservice.
     */
    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to "
                + "validate CDA documents using model based validation.\n";
        res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
        return res;
    }

    /**
     * Return a DetailedResult as a String
     *
     * @param dr : DetailedResult
     * @return String containing the dr
     */
    private String getDetailedResultAsString(DetailedResult dr) {
        if (dr != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                DetailedResultTransformer.save(baos, dr);
                String res = baos.toString();
                res = deleteUnicodeZero(res);
                return res;
            } catch (JAXBException e) {
                log.error("Problem to parse the result of validation", e);
            }
        }
        return null;
    }

    private DetailedResult getStringAsDetailedResult(String dr) {
        File cdaFile = null;
        JAXBContext jaxbContext;
        DetailedResult detailedResult = null;
        try {
            cdaFile = File.createTempFile("cdaBbrToValidate", ".xml");
            FileReadWrite.printDoc(dr, cdaFile.getAbsolutePath());

            jaxbContext = JAXBContext.newInstance(DetailedResult.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            detailedResult = (DetailedResult) jaxbUnmarshaller.unmarshal(cdaFile);

        } catch (JAXBException e) {
            log.error("failed to marshall temp file :" + e.getMessage());
        } catch (IOException e) {
            log.error("failed to create temp file :" + e.getMessage());
        }

        return detailedResult;
    }

    private static String deleteUnicodeZero(String s) {
        return s.replaceAll("[\\u0000]", "");
    }

    private List<String> getListOfValidators() {
        List<String> res = new ArrayList<>();
        Validators[] dd = Validators.values();
        for (Validators validators : dd) {
            res.add(validators.getValue());
        }
        List<String> lbbr = BBRUtil.getListActiveBBR();
        res.addAll(lbbr);
        return res;
    }

    /**
     * Validate a CDA document based on the validator name.
     *
     * @param document  : the CDA document to be validated
     * @param validator : the validator name
     * @return A string containing the validation result
     */
    @WebMethod
    @WebResult(name = "DetailedResult")
    public String validateDocument(@WebParam(name = "document") String document,
                                   @WebParam(name = "validator") String validator) throws SOAPException {
        Validators val = Validators.getValidatorFromValue(validator);
        boolean isABBRValidator = ValidatorDescriber.isBbr(validator);
        if (val == null && !isABBRValidator) {
            throw new SOAPException("The validator shall be from this list : " + this.getListOfValidators());
        } else if (val != null) {
            DetailedResult dr = null;
            switch (val) {
                case IHE_XDLAB:
                    dr = MicroDocumentValidation.validateIHEXDLAB(document);
                    break;
                case IHE_BASIC_CDA:
                    dr = MicroDocumentValidation.validateIHEBASICCDA(document);
                    break;
                case IHE_BASIC_CDA_STRICT:
                    dr = MicroDocumentValidation.validateIHEBASICCDASTRICTED(document);
                    break;
                case EPSOS_ED_PIVOT:
                    dr = MicroDocumentValidation.validateEPSOSEDPIVOT(document);
                    break;
                case EPSOS_EP_PIVOT:
                    dr = MicroDocumentValidation.validateEPSOSEPPIVOT(document);
                    break;
                case EPSOS_PS_PIVOT:
                    dr = MicroDocumentValidation.validateEPSOSPSPIVOT(document);
                    break;
                case EPSOS_ED_FRIENDLY:
                    dr = MicroDocumentValidation.validateEPSOSEDFRIENDLY(document);
                    break;
                case EPSOS_EP_FRIENDLY:
                    dr = MicroDocumentValidation.validateEPSOSEPFRIENDLY(document);
                    break;
                case EPSOS_PS_FRIENDLY:
                    dr = MicroDocumentValidation.validateEPSOSPSFRIENDLY(document);
                    break;
                case EPSOS_HCER:
                    dr = MicroDocumentValidation.validateEPSOSHCER(document);
                    break;
                case EPSOS_MRO:
                    dr = MicroDocumentValidation.validateEPSOSMRO(document);
                    break;
                case EPSOS_SCAN:
                    dr = MicroDocumentValidation.validateEPSOSSCAN(document);
                    break;
                case IHE_XDS_SD_XDSIB:
                    dr = MicroDocumentValidation.validateIHEXDSSDXDSIB(document);
                    break;
                case EPSOS_CONS:
                case IHE_BPPC:
                    dr = MicroDocumentValidation.validateIHEBPPC(document);
                    break;
                case ASIP_CDA_MIN:
                    dr = MicroDocumentValidation.validateASIPCDAMIN(document);
                    break;
                case ASIP_FRCP:
                    dr = MicroDocumentValidation.validateASIPFRCP(document);
                    break;
                case CCD_BASIC:
                    dr = MicroDocumentValidation.validateCCDBASIC(document);
                    break;
                case IHE_CRC:
                    dr = MicroDocumentValidation.validateIHECRC(document);
                    break;
                case LUX_HEADER:
                    dr = MicroDocumentValidation.validateLUXHEADER(document);
                    break;
                case LUX_BODYL1:
                    dr = MicroDocumentValidation.validateLUXBODYL1(document);
                    break;
                case KSA_HEADER:
                    dr = MicroDocumentValidation.validateKSAHEADER(document);
                    break;
                case KSA_BPPC:
                    dr = MicroDocumentValidation.validateKSABPPC(document);
                    break;
                case KSA_DRR:
                    dr = MicroDocumentValidation.validateKSADRR(document);
                    break;
                case KSA_BSRR:
                    dr = MicroDocumentValidation.validateKSABSRR(document);
                    break;
                case KSA_LABR:
                    dr = MicroDocumentValidation.validateKSALABR(document);
                    break;
                case KSA_LABO:
                    dr = MicroDocumentValidation.validateKSALABO(document);
                    break;
                case ACC_RCS_C:
                    dr = MicroDocumentValidation.validateACCRCSC(document);
                    break;
                case ACC_RCS_EP:
                    dr = MicroDocumentValidation.validateACCRCSEP(document);
                    break;
                case ANAPATH_APSR:
                    dr = MicroDocumentValidation.validateANAPATHAPSR(document);
                    break;
                case EXPAND_CDA:
                    dr = MicroDocumentValidation.validateEXPANDCDA(document);
                    break;
                default:
                    throw new SOAPException("The validator shall be from this list : " + this.getListOfValidators());
            }
            return cleanResult(dr);
        } else {
            BuildingBlockRepository bbr = BuildingBlockRepository.getBbrFromLabel(validator);
            JavaVersionEnum javaVersion = bbr.getJavaVersion();
            DetailedResult dr = null;
            String res = "";
            try {
                res = ProcessBBR.processBbrValidation(bbr, document, javaVersion);
                if(res != null){
                    res = res.trim();
                }
                int seconds = 10;
                int iteration = 0;
                while ((res == null || res.isEmpty()) && iteration<2){
                    log.warn("couldn't validate document, attempt {}",iteration+1);
                    log.warn("Waiting {} seconds before retrying...",seconds);

                    //wait 10s before retrying new validation
                    Thread.sleep(seconds*1000);
                    res = ProcessBBR.processBbrValidation(bbr, document,javaVersion);
                    if(res != null){
                        res = res.trim();
                    }
                    log.warn("res: {}",res);
                    iteration++;
                }
                if(iteration == 2){
                    log.error("Validation couldn't be performed after 2 tries");
                }
                dr = getStringAsDetailedResult(res);
                MicroDocumentValidation.summarizeBbrDetailedResult(dr, validator);
                res = cleanResult(dr);
            } catch (IOException | InterruptedException e) {
                log.error("Problem to procees the validation", e);
            }
            return res;
        }
    }

    private String cleanResult(DetailedResult dr) {
        String res;
        res = this.getDetailedResultAsString(dr);
        if (res != null && res.contains("?>")) {
            res = res.substring(res.indexOf("?>") + 2);
        }
        return res;
    }

    /**
     * Return the list of validators based on a discriminator.
     * Example : if the discriminator is IHE, we will have all the validators'names *
     * containing IHE in the name of the validator
     *
     * @param descriminator : the descriminator that will be used for filtering validation tools
     * @return A list of string : the names of the filtered validators
     */
    @WebMethod
    @WebResult(name = "Validators")
    public List<String> getListOfValidators(@WebParam(name = "descriminator") String descriminatorParam)
            throws SOAPException {
        String descriminator = descriminatorParam;
        if (descriminator == null) {
            descriminator = "";
        }
        List<String> res = new ArrayList<>();
        res.add(Validators.IHE_BASIC_CDA.getValue());
        if ("ihe".equalsIgnoreCase(descriminator)) {
            res.add(Validators.IHE_BASIC_CDA_STRICT.getValue());
        }
        for (Validators val : Validators.values()) {
            if (val.getValue().toLowerCase().contains(descriminator.toLowerCase()) && val.getIsActive()) {
                res.add(val.getValue());
            }
        }
        BuildingBlockRepositoryQuery bbrListQuery = new BuildingBlockRepositoryQuery();
        bbrListQuery.active().eq(true);
        List<BuildingBlockRepository> allBbr = bbrListQuery.getList();
        for (BuildingBlockRepository bbr : allBbr) {
            if (bbr.getLabel().toLowerCase().contains(descriminator.toLowerCase())) {
                res.add(bbr.getLabel());
            }
        }

        return res;
    }

    @Override
    public List<String> getListSupportedMediaTypes() {
        return SupportedMediaType.getListSupportedMediaType();
    }

}
