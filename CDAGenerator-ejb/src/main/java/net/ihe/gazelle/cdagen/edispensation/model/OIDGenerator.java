/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.cdagen.edispensation.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;

/**
 * @class			OIDGenerator
 * @author			Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @author          Abderrazek Boufahja / INRIA Rennes IHE development Project
 * 
 *
 */

@Entity
@Name("oidGenerator")
@Table(name="cdagen_oid", schema="public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cdagen_oid_sequence", sequenceName = "cdagen_oid_id_seq", allocationSize=1)
public class OIDGenerator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 831235001748497654L;

	private static final String rootOidPreferenceName = "root_oid";
	
	@Id
	@NotNull
	@Column(name="id", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cdagen_oid_sequence")
	private Integer id;
	
	/**
	 * Constructor
	 */

	public OIDGenerator(){
		
	}
	
	public OIDGenerator(Integer id){
		this.id = id;
	}
	
	/**
	 * Getter and Setter
	 */
	public Integer getId(){
		return this.id;
	}
	
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	public static String getNewOid()
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		OIDGenerator oid = new OIDGenerator();
		oid = em.merge(oid);
		em.flush();
		return getRootOid().concat(Integer.toString(oid.getId()));
	}
	
	private static String getRootOid()
	{
		return ApplicationPreferenceManager.getStringValue(rootOidPreferenceName);
	}
}
