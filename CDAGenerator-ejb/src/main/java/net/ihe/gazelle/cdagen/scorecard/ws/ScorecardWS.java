package net.ihe.gazelle.cdagen.scorecard.ws;

import net.ihe.gazelle.cdagen.simulator.common.utils.DecodeUtil;
import net.ihe.gazelle.cdagen.validator.ws.DetailedResultTransformer;
import net.ihe.gazelle.validation.DetailedResult;
import org.apache.tika.Tika;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

/**
 * <b>Class Description : </b>ScorecardWS<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 06/12/16
 * @class ScorecardWS
 * @package net.ihe.gazelle.cda.scorecard.ws
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@Stateless
@Name("scorecardws")
public class ScorecardWS implements ScorecardWSLocal {


    public static final String GAZELLE_CDA_VALIDATION = "Gazelle CDA Validation : ";
    private static Logger log = LoggerFactory.getLogger(ScorecardWS.class);

    @Override
    public Response hello(@Context HttpServletRequest req) {
        return Response.ok("hello").build();
    }

    @Override
    public Response computeScorecard(String b64DetailedResult, @QueryParam("oid") String oid) {
        return null;
    }

    @Override
    public Response getScorecard(@QueryParam("oid") String oid) {
        return null;
    }

    @Override
    public Response computeAndReturnScorecard(String b64DetailedResult, @QueryParam("oid") String oid) {
        Response.ResponseBuilder responseBuilder;

        if (b64DetailedResult == null || b64DetailedResult.isEmpty()) {
            responseBuilder = Response.status(Response.Status.BAD_REQUEST);
            responseBuilder.header("Warning", "detailedResult parameter cannot be null nor empty");
            return responseBuilder.build();
        } else {
            byte[] buffer = DatatypeConverter.parseBase64Binary(b64DetailedResult);
            if (buffer != null) {
                Tika tika = new Tika();
                String mediaType = tika.detect(buffer);
                log.info("detected mediaType: " + mediaType);
                if (mediaType.equals("application/zip")) {
                    try {
                        String document = DecodeUtil.decodeDocument(buffer, mediaType);
                        buffer = document.getBytes("UTF-8");
                    } catch (SOAPException | UnsupportedEncodingException e) {
                        log.error("Failed to decode ", e.getMessage());
                    }
                }
                ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
                DetailedResult result;
                try {
                    result = DetailedResultTransformer.load(bais);
                    String validator = result.getValidationResultsOverview().getValidationServiceName();
                    // in old report version, validator engine "Gazelle CDA Validator: " is concatenated with
                    // the name of the validator, retrieve the validator name in that case
                    if (validator == null || validator.isEmpty()) {
                        responseBuilder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
                        responseBuilder.header("Warning", "Validator name is not available in validation result, cannot create scorecard");
                        return responseBuilder.build();
                    } else if (validator.contains(GAZELLE_CDA_VALIDATION)) {
                        validator = validator.replace(GAZELLE_CDA_VALIDATION, "");
                    }
                    try {
                        String scorecard = ScoringJob.computeScorecard(result, validator, oid);
                        responseBuilder = Response.status(Response.Status.OK);
                        responseBuilder.entity(scorecard);
                    } catch (Exception e) {
                        responseBuilder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
                        responseBuilder.header("Warning", e.getMessage());
                        log.error(e.getMessage(), e);
                    }
                    return responseBuilder.build();
                } catch (JAXBException e) {
                    responseBuilder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
                    responseBuilder.header("Warning", "Cannot read received detailed result: " + e.getMessage());
                    return responseBuilder.build();
                }
            } else {
                responseBuilder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
                responseBuilder.header("Warning", "Cannot decode detailed result, shall be sent as Base64 string");
                return responseBuilder.build();
            }
        }
    }
}
