package net.ihe.gazelle.cdagen.bbr.action;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import org.apache.commons.io.FileUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.action.ConstraintDefManager;
import net.ihe.gazelle.cdagen.bbr.model.BuildingBlockRepository;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.providers.EntityManagerService;

/**
 * Created by xfs on 25/11/16.
 */

@Name("buildingBlockRepositoryListManager")
@Scope(ScopeType.PAGE)
public class BuildingBlockRepositoryListManager {

    private static Logger log = LoggerFactory.getLogger(BuildingBlockRepositoryListManager.class);
    @In(create = true, required = false, value = "processBBR")
    ProcessBBRInterface bbrProcessor;
    @In(create = true, required = true, value = "consDefManager")
    ConstraintDefManager cdm;
    private BuildingBlockRepositoryFilter filter;
    private FilterDataModel<BuildingBlockRepository> buildingBlockRepositoryList;

    public ProcessBBRInterface getBbrProcessor() {
        return bbrProcessor;
    }

    public void setBbrProcessor(ProcessBBRInterface bbrProcessor) {
        this.bbrProcessor = bbrProcessor;
    }

    public BuildingBlockRepositoryFilter getFilter() {
        if (filter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            filter = new BuildingBlockRepositoryFilter(requestParameterMap);
        }
        return filter;
    }

    public FilterDataModel<BuildingBlockRepository> getBuildingBlockRepositoryList() {
        if (buildingBlockRepositoryList == null) {
            buildingBlockRepositoryList = new FilterDataModel<BuildingBlockRepository>(getFilter()) {
                /**
                 *
                 */
                private static final long serialVersionUID = 1L;

                @Override
                protected Object getId(BuildingBlockRepository t) {
                    return t.getId();
                }
            };
        }
        return buildingBlockRepositoryList;
    }

    public void clearFilter() {
        getFilter().clear();
    }

    public void build(Integer bbrId) {
        log.info("hello build ");
        EntityManager em = EntityManagerService.provideEntityManager();
        BuildingBlockRepository bbr = em.find(BuildingBlockRepository.class, bbrId);
        System.out.println("bbr : " + bbr);
        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        boolean processable = checkIfProcessable(bbrPathString);
        log.info("processable : " + processable);
        log.info("bbrPathString : " + bbrPathString);
        if (processable) {
            try {
                log.info("bbr : " + bbrProcessor);
                bbrProcessor.processJarValidation(bbrPathString, bbr);
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Problem to process exec");
                log.error("problem to process exec", e);
            } catch (InterruptedException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Problem to process exec, InterruptedExeption");
                log.error("problem to process exec, InterruptedExeption", e);
            }
        }
    }

    public void build(BuildingBlockRepository bbr) {
        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        boolean processable = checkIfProcessable(bbrPathString);
        if (processable) {
            try {
                bbrProcessor.processJarValidation(bbrPathString, bbr);
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Problem to process exec");
                log.error("problem to process exec", e);
            } catch (InterruptedException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Problem to process exec, InterruptedExeption");
                log.error("problem to process exec, InterruptedExeption", e);
            }
        }
    }

    private boolean checkIfProcessable(String bbrPathString) {
        boolean processable = true;
        if (bbrPathString == null) {
            processable = false;
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "The bbr_folder application setting is not set, build won't work.");
        } else {
            File bbrPath = new File(bbrPathString);
            if (!bbrPath.exists() || !bbrPath.isDirectory()) {
                processable = false;
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, bbrPathString + " is not a valid directory");
            } else {
                File bbrArchivesPath = new File(bbrPathString + "/generatedValidators/archives/");
                if (!bbrArchivesPath.exists() && !bbrArchivesPath.mkdir()) {
                    processable = false;
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Impossible to create the folder " + bbrArchivesPath.getAbsolutePath());
                }
                File generateValidatorShPath = new File(
                        bbrPathString + "/generator/hl7templates-packager-app/generateValidator.sh");
                if (!generateValidatorShPath.exists()) {
                    processable = false;
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "generateValidator.sh is not found at "
                            + generateValidatorShPath.getAbsolutePath() + ". Can't process");
                }
            }
        }
        return processable;
    }

    public void deployDocumentation(BuildingBlockRepository bbr) {

        String bbrPathString = ApplicationPreferenceManager.getStringValue("bbr_folder");
        File documentationPath = new File(
                bbrPathString + "/generatedValidators/execs/" + bbr.getId() + "/documentation/");
        if (!documentationPath.exists()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The documentation folder does not exist");
        } else {
            File[] constraintsFiles = documentationPath.listFiles();
            if (constraintsFiles == null || constraintsFiles.length == 0) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The documentation folder is empty");
            } else {
                for (File currentFile : constraintsFiles) {
                    String constraintsString = null;
                    try {
                        constraintsString = FileUtils.readFileToString(currentFile);
                        cdm.deleteAndGenerate(constraintsString);
                    } catch (IOException e) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Can't deploy the documentation");
                        log.error("error to read the constraints file", e);
                    }
                }
            }
        }

    }

}
