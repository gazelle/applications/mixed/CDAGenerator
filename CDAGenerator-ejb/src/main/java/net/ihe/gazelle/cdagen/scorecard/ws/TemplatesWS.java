package net.ihe.gazelle.cdagen.scorecard.ws;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.cdagen.bbr.model.TCDescriber;
import net.ihe.gazelle.cdagen.bbr.model.TCDescriberQuery;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.marshaller.ObjectMarshallerImpl;
import net.ihe.gazelle.scoring.freemind.model.Map;
import net.ihe.gazelle.scoring.freemind.model.Node;
import net.ihe.gazelle.scoring.model.templates.TemplatesContainment;
import net.ihe.gazelle.scoring.richness.util.MMExtractor;
import net.ihe.gazelle.scoring.richness.util.TemplatesContainmentMarshaller;

/**
 * <b>Class Description : </b>TemplatesWS<br>
 * <br>
 *
 * @author Abderrazek Boufahja / IHE-Europe development Project
 * @class TemplatesWS
 * @package net.ihe.gazelle.cda.scorecard.ws
 */

@Stateless
@Name("templates")
public class TemplatesWS implements TemplatesWSLocal {


    public static final String GAZELLE_CDA_VALIDATION = "Gazelle CDA Validation : ";
    private static Logger log = LoggerFactory.getLogger(TemplatesWS.class);

    @Override
    public Response hello(@Context HttpServletRequest req) {
        return Response.ok("hello").build();
    }

	@Override
	public String computeTemplatesAsMM(String templateId, String tcName, String target) {
		TCDescriberQuery tcq = new TCDescriberQuery();
		tcq.tcName().eq(tcName);
		TCDescriber tcd = tcq.getUniqueResult();
		try {
			if (tcd != null) {
				TemplatesContainment tc = null;
				if ("original".equals(target)) {
					tc = TemplatesContainmentMarshaller.loadTemplatesContainment(
							new ByteArrayInputStream(tcd.getTcContent()));
					
				}
				else {
					tc = TemplatesContainmentMarshaller.loadTemplatesContainment(
							new ByteArrayInputStream(tcd.getTcFlattened()));
				}
				if (tc != null) {
					Map mm = MMExtractor.extractMMAsMap(tc, templateId);
					if (mm.getNode() != null) {
						for (Object obj : mm.getNode().getArrowlinkOrCloudOrEdge()) {
							updateNodeToCollapse(obj);
						}
					}
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					new ObjectMarshallerImpl<Map>(Map.class).marshall(mm, baos);
					return updateMM(baos.toString());
				}
			}
			else {
				log.error("problem to find tcd : '" + tcName + "'");
			}
		} 
		catch(Exception e) {
			log.error("problem to treat the request", e);
		}
        return null;
		
	}

	private void updateNodeToCollapse(Object obj) {
		if (obj instanceof Node) {
			Node node = (Node)obj;
			node.setFOLDED("true");
		}
	}

	private String updateMM(String mmPam) {
		String mm = mmPam.substring(mmPam.indexOf("?>") + 2).trim();
		return "<?xml-stylesheet type=\"text/xsl\" href=\"" + ApplicationPreferenceManager.getStringValue("mm_xsl") + "\"?>\n" + mm;
	}

    
}
