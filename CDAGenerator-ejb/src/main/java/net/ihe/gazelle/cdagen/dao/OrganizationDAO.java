package net.ihe.gazelle.cdagen.dao;

import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.cdagen.model.Organization;
import net.ihe.gazelle.hql.HQLQueryBuilder;

public class OrganizationDAO {
    
    public static Organization mergeOrganization(Organization pp, EntityManager em){
    	Organization res = em.merge(pp);
        em.flush();
        return res;
    }
    
    public static Organization getOrganizationByKeyword(String keyword, EntityManager em){
        HQLQueryBuilder<Organization> hql = new HQLQueryBuilder<Organization>(em, Organization.class);
        hql.addEq("keyword", keyword);
        List<Organization> lorg = hql.getList();
        if ((lorg != null)&& (!lorg.isEmpty())){
            return lorg.get(0);
        }
        return null;
    }

}
