package net.ihe.gazelle.cdagen.validator.ws;

public enum Validators {

	IHE_XDLAB("IHE - LAB - Sharing Laboratory Reports (XD-LAB) (old version)"),
	IHE_BASIC_CDA("HL7 - CDA Release 2"),
	IHE_BASIC_CDA_STRICT("HL7 - CDA Release 2 (strict)"),
	EPSOS_PS_PIVOT("epSOS - Patient Summary Pivot"),
	EPSOS_EP_PIVOT("epSOS - ePrescription Pivot"),
	EPSOS_ED_PIVOT("epSOS - eDispensation Pivot"),
	EPSOS_PS_FRIENDLY("epSOS - Patient Summary Friendly"),
	EPSOS_EP_FRIENDLY("epSOS - ePrescription Friendly"),
	EPSOS_ED_FRIENDLY("epSOS - eDispensation Friendly"),
	EPSOS_CONS("epSOS - eConsent"),
	EPSOS_SCAN("epSOS - Scanned Document", false),
	EPSOS_HCER("epSOS - HCER HealthCare Encounter Report"),
	EPSOS_MRO("epSOS - MRO Medication Related Overview"),
//	IHE_XDS_SD("IHE - ITI - Cross-Enterprise Sharing of Scanned Documents (XDS-SD)"),
	IHE_XDS_SD_XDSIB("IHE - RAD - CDA document wrapper (XDS-I.b)"),
	IHE_BPPC("IHE - ITI - Basic Patient Privacy Consent (BPPC)"),
	ASIP_CDA_MIN("ASIP - CDA Structuration minimale"),
	ASIP_FRCP("ASIP - Fiche de Réunion de Concertation Pluridisciplinaire (FRCP)"),
	CCD_BASIC("HL7 - CCD"),
//	IHE_PCC_BASIC("IHE - PCC - Common templates"),
	EXPAND_CDA("EXPAND - CDA documents [informal testing - art-decor based requirements]"),
	IHE_CRC("IHE - CARD - Cath Report Content (CRC)"),
	LUX_HEADER("LUX - Header Specifications"),
	LUX_BODYL1("LUX - Body Level1 Specifications"),
	KSA_HEADER("KSA - Header Specifications"),
	KSA_BPPC("KSA - Basic Patient Privacy Consents"),
	KSA_DRR("KSA - XDS-I.b Displayable Radiology report"),
	KSA_LABR("KSA - Laboratory Report"),
	KSA_LABO("KSA - Laboratory Order"),
	KSA_BSRR("KSA - XDS-I.b Basic Structured Radiology report"),
	ACC_RCS_C("IHE - CARD - Registry Content Submission (RCS-C)"),
	ACC_RCS_EP("IHE - CARD - Registry Content Submission - Electrophysiology (RCS-EP)"),
	ANAPATH_APSR("IHE - PATH - Anatomic Pathology Structured Reports (APSR)");
//	IHE_PHARM_PRE_AD("IHE - PHARM - Pharmacy Prescription (PRE)"),
//	IHE_PHARM_DIS_AD("IHE - PHARM - Pharmacy Dispense (DIS)"),
//	IHE_PHARM_PADV_AD("IHE - PHARM - Pharmacy Pharmaceutical Advice (PADV)"),;
	
//	CCDA1("HL7 - C-CDA R1.1"),
//	CCDA2("HL7 - C-CDA R2.1"),
//	EYE_GEE_PN("IHE - EYE - GEE Progress Note"),
//	EYE_SUM("IHE - EYE - Summary Record (EC-Summary)"),
//	ABRUMET_RS_AD("ABRUMET - Referral Summary")

	private Validators(String val) {
		this.value = val;
	}
	
	private Validators(String val, Boolean isActive) {
		this.value = val;
		this.isActive = isActive;
	}
	
	private Boolean isActive = true;

	private String value;

	public Boolean getIsActive() {
		return isActive;
	}

	public String getValue() {
		return value;
	}

	public static Validators getValidatorFromValue(String value) {
		for (Validators val : Validators.values()) {
			if (val.value.equals(value))
				return val;
		}
		return null;
	}

}
