package net.ihe.gazelle.cdagen.simulator.common.utils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.DatatypeConverter;
import javax.xml.soap.SOAPException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class DecodeUtil {
	
	private DecodeUtil() {
		// private constructor
	}
	
	public static final int BUFFER_SIZE = 8192;
	
	private static Logger log = LoggerFactory.getLogger(DecodeUtil.class);
	
	public static String decodeDocument(byte[] buffer, String mediaTypeParam) throws SOAPException {
		String mediaType = mediaTypeParam;
		if (mediaType == null) {
			mediaType = "text/plain";
		}
		if (mediaType.equals("application/zip")) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				unzip(new ByteArrayInputStream(buffer), baos);
			}
			catch(Exception e) {
				log.error("error to read stream", e);
				return null;
			}
			return baos.toString();
		}
		else if (mediaType.equals("text/plain") || mediaType.equals("application/xml")) {
			try {
				return new String(buffer, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				log.error("Prolem to create a string res", e.getMessage());
				return null;
			}
		}
		else {
			throw new SOAPException("mediaType provided is not supported by the tool :'" + mediaType + "'");
		}
	}
	
	public static void encodeDocument(InputStream is, OutputStream os, String resultMediaType) 
			throws IOException, SOAPException {
		if (resultMediaType != null) {
			if ( resultMediaType.equals("application/zip")) {
				zip(is, os);
			}
			else if (resultMediaType.equals("text/plain")) {
				IOUtils.copy(is, os);
			}
			else {
				throw new SOAPException("mediaType provided is not supported by the tool :'" + resultMediaType + "'");
			}
		}
	}
	
	private static void zip(InputStream in, OutputStream os) throws IOException {
		if (os != null && in != null) {
			byte[] buffer = new byte[8192];
			int read = 0;
			try (ZipOutputStream zos = new ZipOutputStream(os);) {
				ZipEntry entry = new ZipEntry("mbvalResult.xml");
				zos.putNextEntry(entry);
				while (-1 != (read = in.read(buffer))) {
					zos.write(buffer, 0, read);
				}
				in.close();
			}
		}
	}

	public static final void unzip(InputStream is, OutputStream os) throws IOException {
		try (ZipInputStream archive = new ZipInputStream(is)) {
			ZipEntry entry;
			while ((entry = archive.getNextEntry()) != null)
			{
				log.info("entry: " + entry.getName() + ", " + entry.getSize());
				BufferedOutputStream out = new BufferedOutputStream(os);

				byte[] buffer = new byte[BUFFER_SIZE];
				int read;
				while (-1 != (read = archive.read(buffer))) {
					out.write(buffer, 0, read);
				}
				out.close();
			}
		}

	}

}
