package net.ihe.gazelle.cdagen.eprescription.action;

import java.io.Serializable;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.richfaces.event.DropEvent;

import net.ihe.gazelle.cdagen.model.Organization;
import net.ihe.gazelle.cdagen.model.Patient;
import net.ihe.gazelle.cdagen.simulator.common.model.Concept;
import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.patient.PatientGenerator;

@Stateful
@Name("ePrescManager")
@Scope(ScopeType.SESSION)
public class EPrescManager extends PatientGenerator implements EPrescManagerLocal,Serializable{
    
    @Logger
    private static Log log;
    
    private Patient selectedAuthor;
    
    private Organization selectedAuthorOrganization;
    
    private Concept selectedAuthorRole;
    
    private Patient selectedPatient;
    
    private Organization selectedPatientOrganization;
    
    private String ddsDisplayMode = "List Persons";

    public String getDdsDisplayMode() {
        return ddsDisplayMode;
    }

    public void setDdsDisplayMode(String ddsDisplayMode) {
        this.ddsDisplayMode = ddsDisplayMode;
    }

    public Patient getSelectedAuthor() {
        return selectedAuthor;
    }

    public void setSelectedAuthor(Patient selectedAuthor) {
        this.selectedAuthor = selectedAuthor;
    }

    public Organization getSelectedAuthorOrganization() {
        return selectedAuthorOrganization;
    }

    public void setSelectedAuthorOrganization(
            Organization selectedAuthorOrganization) {
        this.selectedAuthorOrganization = selectedAuthorOrganization;
    }

    public Concept getSelectedAuthorRole() {
        return selectedAuthorRole;
    }

    public void setSelectedAuthorRole(Concept selectedAuthorRole) {
        this.selectedAuthorRole = selectedAuthorRole;
    }

    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    public Organization getSelectedPatientOrganization() {
        return selectedPatientOrganization;
    }

    public void setSelectedPatientOrganization(
            Organization selectedPatientOrganization) {
        this.selectedPatientOrganization = selectedPatientOrganization;
    }
    
    public void processDropForAuthor(DropEvent dropevent) {
        log.info("welcome !");
        Integer ss = (Integer) dropevent.getDragValue();
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.selectedAuthor = entityManager.find(Patient.class, ss);
    }
    
    public void processDropForPatient(DropEvent dropevent) {
        Integer ss = (Integer) dropevent.getDragValue();
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        this.selectedPatient = entityManager.find(Patient.class, ss);
    }
    
    public void generateAndSaveAuthor(){
        AbstractPatient ap = this.generatePatient();
        this.selectedAuthor = new Patient(ap);
    }
    
    public void generateAndSavePatient(){
        AbstractPatient ap = this.generatePatient();
        this.selectedPatient = new Patient(ap);
    }
    
    @Destroy
    @Remove
    public void destroy()
    {
        log.info("destroy");
    }

    
}
