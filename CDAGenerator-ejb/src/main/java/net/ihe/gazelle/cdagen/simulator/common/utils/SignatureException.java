package net.ihe.gazelle.cdagen.simulator.common.utils;

public class SignatureException extends Exception {

	private static final long serialVersionUID = -5100871848771290637L;

	public SignatureException() {
		super();
	}

	public SignatureException(String message, Throwable cause) {
		super(message, cause);
	}

	public SignatureException(String message) {
		super(message);
	}

	public SignatureException(Throwable cause) {
		super(cause);
	}

}
