package net.ihe.gazelle.cdagen.action;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

@Name("CDAToolsMan")
@Scope(ScopeType.PAGE)
public class CDAToolsMan implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Logger
	private static Log log;

	private String cdaDoc;

	public String getCdaDoc() {
		return cdaDoc;
	}

	public void setCdaDoc(String cdaDoc) {
		this.cdaDoc = cdaDoc;
	}

	public void uploadEventlistener(FileUploadEvent event) {
		try {
			UploadedFile item = event.getUploadedFile();
			if (item.getData() != null && item.getData().length > 0) {
				this.cdaDoc = new String(item.getData(), StandardCharsets.UTF_8);
			} else {
				FacesMessages.instance().add("File is empty.");
			}
		} catch (Exception e) {
			log.error("uploadEventListener: an error occurred - " + e.getMessage());
			return;
		}
	}

}
