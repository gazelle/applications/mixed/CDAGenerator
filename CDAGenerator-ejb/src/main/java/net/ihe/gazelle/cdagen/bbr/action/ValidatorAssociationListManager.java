package net.ihe.gazelle.cdagen.bbr.action;

import java.util.Map;

import javax.faces.context.FacesContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import net.ihe.gazelle.cdagen.bbr.model.ValidatorDescriber;
import net.ihe.gazelle.common.filter.FilterDataModel;

/**
 * Created by xfs on 07/12/16.
 */

@Name("validatorAssociationListManager")
@Scope(ScopeType.PAGE)
public class ValidatorAssociationListManager {

	private ValidatorDescriberFilter filter;
	private FilterDataModel<ValidatorDescriber> validatorAssociations;

	public ValidatorDescriberFilter getFilter() {
		if (filter == null) {
			final FacesContext fc = FacesContext.getCurrentInstance();
			final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
			filter = new ValidatorDescriberFilter(requestParameterMap);
		}
		return filter;
	}

	public FilterDataModel<ValidatorDescriber> getValidatorAssociations() {
		if (validatorAssociations == null) {
			validatorAssociations = new FilterDataModel<ValidatorDescriber>(getFilter()) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				protected Object getId(ValidatorDescriber t) {
					return t.getId();
				}
			};
		}
		return validatorAssociations;
	}

	public void clearFilter() {
		getFilter().clear();
	}

}
