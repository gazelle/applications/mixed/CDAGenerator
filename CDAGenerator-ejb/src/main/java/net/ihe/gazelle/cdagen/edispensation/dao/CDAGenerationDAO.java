package net.ihe.gazelle.cdagen.edispensation.dao;

import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.cdagen.dao.PatientDAO;
import net.ihe.gazelle.cdagen.edispensation.model.CDAFile;
import net.ihe.gazelle.cdagen.edispensation.model.CDAGeneration;
import net.ihe.gazelle.cdagen.model.Patient;
import net.ihe.gazelle.hql.HQLQueryBuilder;

public final class CDAGenerationDAO {

	private CDAGenerationDAO() {
		// private constructor
	}

	public static CDAGeneration mergeCDAGeneration(CDAGeneration cdagen, EntityManager em) {
		if (cdagen != null) {
			if (cdagen.getPrescription() != null) {
				CDAFile presc = CDAFileDAO.mergeCDAFile(cdagen.getPrescription(), em);
				cdagen.setPrescription(presc);
			}
			if (cdagen.getDispensation() != null) {
				CDAFile disp = CDAFileDAO.mergeCDAFile(cdagen.getDispensation(), em);
				cdagen.setDispensation(disp);
			}
			if (cdagen.getAuthor() != null) {
				Patient pp = PatientDAO.mergePatient(cdagen.getAuthor(), em);
				cdagen.setAuthor(pp);
			}
			CDAGeneration cdagen2 = em.merge(cdagen);
			em.flush();
			return cdagen2;
		}
		return null;
	}

	public static List<CDAGeneration> getListCDAGeneration(EntityManager em) {
		HQLQueryBuilder<CDAGeneration> hqr = new HQLQueryBuilder<>(em, CDAGeneration.class);
		return hqr.getList();
	}

	public static List<CDAGeneration> getListCDAGenerationFiltered(EntityManager em, Patient author) {
		HQLQueryBuilder<CDAGeneration> hqr = new HQLQueryBuilder<>(em, CDAGeneration.class);
		if ((author != null) && (author.getId() != null))
			hqr.addEq("author", author);
		return hqr.getList();
	}

}
