package net.ihe.gazelle.cdagen.bbr.action;

import java.util.Map;

import javax.persistence.EntityManager;

import net.ihe.gazelle.cdagen.bbr.model.ValidatorDescriber;
import net.ihe.gazelle.cdagen.bbr.model.ValidatorDescriberQuery;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;

/**
 * Created by xfs on 07/12/16.
 */
public class ValidatorDescriberFilter extends Filter<ValidatorDescriber> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValidatorDescriberFilter(Map<String, String> requestParameterMap) {
		super(getHQLCriterions(), requestParameterMap);
	}

	private static HQLCriterionsForFilter<ValidatorDescriber> getHQLCriterions() {

		ValidatorDescriberQuery query = new ValidatorDescriberQuery(EntityManagerService.provideEntityManager());
		HQLCriterionsForFilter<ValidatorDescriber> result = query.getHQLCriterionsForFilter();

		result.addPath("validatorName", query.validatorName());
		return result;
	}

	@Override
	public EntityManager getEntityManager() {
		return EntityManagerService.provideEntityManager();
	}

}
