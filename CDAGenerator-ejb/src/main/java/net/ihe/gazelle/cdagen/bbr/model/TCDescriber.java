package net.ihe.gazelle.cdagen.bbr.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBException;

import org.jboss.seam.annotations.Name;

import net.ihe.gazelle.scoring.model.templates.TemplatesContainment;
import net.ihe.gazelle.scoring.richness.util.TemplatesContainmentMarshaller;
import net.ihe.gazelle.scoring.richness.util.TemplatesContainmentUtil;

/**
 * Created by xfs on 07/12/16.
 */

@Entity
@Name("tcDescriber")
@Table(name = "tc_describer")
@SequenceGenerator(name = "tc_describer_sequence", sequenceName = "tc_describer_id_seq", allocationSize = 1)
public class TCDescriber {
	
	
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tc_describer_sequence")
    private int id;

    @Column(name = "tc_name", unique = true, nullable = false)
    private String tcName;
    
    @Column(name = "tc_content")
    private byte[] tcContent;
    
    @Column(name = "tc_flatten")
    private byte[] tcFlattened;
    
    @OneToMany(targetEntity = ValidatorDescriber.class, cascade = CascadeType.ALL, mappedBy = "tcDescriber")
    private List<ValidatorDescriber> validatorDescriberList;
    
    @Column(name="updateDate")
    private Date updateDate;


    public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<ValidatorDescriber> getValidatorDescriberList() {
		return validatorDescriberList;
	}

	public void setValidatorDescriberList(List<ValidatorDescriber> validatorDescriberList) {
		this.validatorDescriberList = validatorDescriberList;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public String getTcName() {
		return tcName;
	}

	public void setTcName(String tcName) {
		this.tcName = tcName;
	}

	public byte[] getTcContent() {
		return tcContent;
	}

	public void setTcContent(byte[] tcContent) {
		this.tcContent = tcContent;
	}

	public byte[] getTcFlattened() {
		return tcFlattened;
	}

	public void setTcFlattened(byte[] tcFlattened) {
		this.tcFlattened = tcFlattened;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tcName == null) ? 0 : tcName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TCDescriber other = (TCDescriber) obj;
		if (tcName == null) {
			if (other.tcName != null)
				return false;
		} else if (!tcName.equals(other.tcName))
			return false;
		return true;
	}
	
	@PrePersist
	@PreUpdate
	public void prePersist() throws JAXBException {
		this.updateDate = new Date();
		if (this.tcContent != null) {
			TemplatesContainment tc = TemplatesContainmentMarshaller.loadTemplatesContainment(
					new ByteArrayInputStream(this.tcContent));
			TemplatesContainment tcf = TemplatesContainmentUtil.flattenTemplatesContainment(tc);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			TemplatesContainmentMarshaller.marshallTemplatesContainment(tcf, baos);
			this.tcFlattened = baos.toByteArray();
		}
	}
    
}
