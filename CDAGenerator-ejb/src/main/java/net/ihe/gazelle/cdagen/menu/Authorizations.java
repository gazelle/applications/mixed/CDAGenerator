package net.ihe.gazelle.cdagen.menu;

import net.ihe.gazelle.common.pages.Authorization;
import org.jboss.seam.security.Identity;

public enum Authorizations implements Authorization {

    ALL,

    LOGGED,

    ADMIN;

    @Override
    public boolean isGranted(final Object... context) {
        switch (this) {
            case ALL:
                return true;
            case LOGGED:
                return Identity.instance().isLoggedIn();
            case ADMIN:
                return Identity.instance().hasRole("admin_role");
            default:
                return false;
        }
    }
}
