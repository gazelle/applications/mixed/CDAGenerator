package net.ihe.gazelle.cdagen.hl7templates.validation;

import java.io.IOException;

import javax.ejb.Local;

import org.jboss.seam.annotations.async.Asynchronous;

@Local
public interface ProcessValidationInterface {

    @Asynchronous
    public void processJarValidation(String path, String pathOutput, String validationExecPath, Integer currentHL7TemplValidationId, boolean reportAllChecks)
    		throws IOException, InterruptedException;
}
