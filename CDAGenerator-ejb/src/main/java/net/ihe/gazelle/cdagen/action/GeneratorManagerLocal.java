package net.ihe.gazelle.cdagen.action;

import javax.ejb.Remove;

import org.jboss.seam.annotations.Destroy;

public interface GeneratorManagerLocal {

	public String getFileToSaveName();

	public void setFileToSaveName(String fileToSaveName);

	public void initSaveAction();

	public void saveCDADoc(String doc);

	public String generateCDAClinicalDocument();

	public String generateCDAClinicalDocument(String cdaDoc);

	public void downloadFile(String string);

	public void validate(String comment);

	@Destroy
	@Remove
	public void destroy();

}