package net.ihe.gazelle.cdagen.action;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import net.ihe.gazelle.cdagen.datamodel.CDAObjectTypeDataModel;
import net.ihe.gazelle.cdagen.model.CDAObjectType;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;

@Stateful
@Name("cdaObjectTypeManager")
@Scope(ScopeType.SESSION)
public class CDAObjectTypeManager implements Serializable, CDAObjectTypeManagerLocal {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Logger
	private static Log log;

	private CDAObjectTypeDataModel foundCDAObjectTypes;

	private CDAObjectType selectedCDAObjectType;

	private Domain selectedDomainForCreators;

	private IntegrationProfile selectedIntegrationProfileForCreator;

	private Actor selectedActorForCreator;

	private ActorIntegrationProfileOption selectedAIPO;

	private IntegrationProfileOption selectedIntegrationProfileOptionForCreator;

	public ActorIntegrationProfileOption getSelectedAIPO() {
		return selectedAIPO;
	}

	public void setSelectedAIPO(ActorIntegrationProfileOption selectedAIPO) {
		this.selectedAIPO = selectedAIPO;
	}

	public CDAObjectTypeDataModel getFoundCDAObjectTypes() {
		if (foundCDAObjectTypes == null)
			foundCDAObjectTypes = new CDAObjectTypeDataModel();
		return foundCDAObjectTypes;
	}

	public void setFoundCDAObjectTypes(CDAObjectTypeDataModel foundCDAObjectTypes) {
		this.foundCDAObjectTypes = foundCDAObjectTypes;
	}

	public CDAObjectType getSelectedCDAObjectType() {
		return selectedCDAObjectType;
	}

	public void setSelectedCDAObjectType(CDAObjectType selectedCDAObjectType) {
		this.selectedCDAObjectType = selectedCDAObjectType;
	}

	public List<Domain> getPossibleDomains() {
		return Domain.getPossibleDomains();
	}

	public List<IntegrationProfile> getPossibleIntegrationProfilesForCreator() {
		return IntegrationProfile.getPossibleIntegrationProfiles(selectedDomainForCreators);
	}

	public Domain getSelectedDomainForCreators() {
		return selectedDomainForCreators;
	}

	public void setSelectedDomainForCreators(Domain selectedDomainForCreators) {
		this.selectedDomainForCreators = selectedDomainForCreators;
	}

	public IntegrationProfile getSelectedIntegrationProfileForCreator() {
		return selectedIntegrationProfileForCreator;
	}

	public void setSelectedIntegrationProfileForCreator(IntegrationProfile selectedIntegrationProfileForCreator) {
		this.selectedIntegrationProfileForCreator = selectedIntegrationProfileForCreator;
	}

	public Actor getSelectedActorForCreator() {
		return selectedActorForCreator;
	}

	public void setSelectedActorForCreator(Actor selectedActorForCreator) {
		this.selectedActorForCreator = selectedActorForCreator;
	}

	public IntegrationProfileOption getSelectedIntegrationProfileOptionForCreator() {
		return selectedIntegrationProfileOptionForCreator;
	}

	public void setSelectedIntegrationProfileOptionForCreator(
			IntegrationProfileOption selectedIntegrationProfileOptionForCreator) {
		this.selectedIntegrationProfileOptionForCreator = selectedIntegrationProfileOptionForCreator;
	}

	public List<Actor> getPossibleActorsForCreator() {
		return Actor.getActorFiltered(null, selectedIntegrationProfileForCreator, null);
	}

	public List<IntegrationProfileOption> getPossibleOptionsForCreator() {
		return IntegrationProfileOption.getListOfIntegrationProfileOptions(selectedActorForCreator,
				selectedIntegrationProfileForCreator);
	}

	public void resetSelectionValuesForCreator(int i) {
		if (i == 1) {
			selectedIntegrationProfileForCreator = null;
			selectedActorForCreator = null;
			selectedIntegrationProfileOptionForCreator = null;
		}
		if (i == 2) {
			selectedActorForCreator = null;
			selectedIntegrationProfileOptionForCreator = null;
		}
		if (i == 3) {
			selectedIntegrationProfileOptionForCreator = null;
		}
	}

	public void addNewCreatorOfcurrentObjectType() {
		ActorIntegrationProfileOption aipo = ActorIntegrationProfileOption.getActorIntegrationProfileOption(
				selectedActorForCreator, selectedIntegrationProfileForCreator,
				selectedIntegrationProfileOptionForCreator);
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		this.selectedCDAObjectType.addAIPO(aipo);
		this.selectedCDAObjectType = em.merge(this.selectedCDAObjectType);
		em.flush();
	}

	public String editCDAObjectType(CDAObjectType cdaobj) {
		this.selectedCDAObjectType = cdaobj;
		return "/cdaObjectType/editCDAObjectType.xhtml";
	}

	public void deleteSelectedAIPOFromCreator() {
		if (this.selectedAIPO != null && this.selectedCDAObjectType != null) {
			this.selectedCDAObjectType.getActorIntegrationProfileOptions().remove(selectedAIPO);
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			this.selectedCDAObjectType = em.merge(this.selectedCDAObjectType);
			em.flush();
			FacesMessages.instance().add(
					"Selected ActorIntegrationProfileOption is deleted from the list of creator of the current CDA Type.");
		}
		this.selectedAIPO = null;
	}

	@Destroy
	@Remove
	public void destroy() {
		log.info("destroy");
	}
}
