package net.ihe.gazelle.cdagen.edispensation.action;

import javax.ejb.Local;

import net.ihe.gazelle.cdagen.datamodel.PatientDataModel;
import net.ihe.gazelle.cdagen.model.Patient;

@Local
public interface AuthorManagerLocal {

    public abstract Patient getSelectedAuthor();

    public abstract void setSelectedAuthor(Patient selectedAuthor);

    public abstract PatientDataModel getAuthorsFounds();

    public abstract void setAuthorsFounds(PatientDataModel authorsFounds);

    public abstract boolean isSearchForIdentity();

    public abstract void setSearchForIdentity(boolean searchForIdentity);

    public abstract void displayAuthor(Patient inPatient);
    
    public void destroy();

}