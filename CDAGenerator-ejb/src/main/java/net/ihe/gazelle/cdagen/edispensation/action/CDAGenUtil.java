package net.ihe.gazelle.cdagen.edispensation.action;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.jboss.seam.Component;
import org.jfree.util.Log;

import net.ihe.gazelle.cdagen.tools.XSLTransformer;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;

public final class CDAGenUtil {
	
	private CDAGenUtil() {
		// private constructor
	}
	
	public static String getCDAModified2(String cda){
        if (cda != null){
            String res = "";
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            String epsosXSL = ApplicationPreferenceManager.getStringValue("epsos_xsl");
            try {
            	Map<String, Object> mapTrans = new HashMap<>();
            	mapTrans.put("uuid", UUID.randomUUID().toString());
				res = XSLTransformer.resultTransformation(cda, new URL(epsosXSL), mapTrans, entityManager);
			} catch (MalformedURLException e) {
				Log.error("the url provided is malformed", e);
			}
            return res;
        }
        return null;
    }

}
