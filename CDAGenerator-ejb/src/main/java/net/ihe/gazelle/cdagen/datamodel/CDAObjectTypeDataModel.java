package net.ihe.gazelle.cdagen.datamodel;

import net.ihe.gazelle.cdagen.model.CDAObjectType;
import net.ihe.gazelle.cdagen.model.CDAObjectTypeQuery;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;

public class CDAObjectTypeDataModel extends FilterDataModel<CDAObjectType> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CDAObjectTypeDataModel() {
		super(new Filter<CDAObjectType>(getCriterionList()));
	}

	private static HQLCriterionsForFilter<CDAObjectType> getCriterionList() {
		CDAObjectTypeQuery query = new CDAObjectTypeQuery();
		return query.getHQLCriterionsForFilter();
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<CDAObjectType> queryBuilder) {
		// nothin to append
	}

	@Override
	protected Object getId(CDAObjectType t) {
		return t.getId();
	}
}
