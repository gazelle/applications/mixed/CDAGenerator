package net.ihe.gazelle.cdagen.action;

import java.io.Serializable;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import net.ihe.gazelle.cdagen.model.Home;

@Name("homeManagerBean")
@Scope(ScopeType.PAGE)
public class HomeManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1111092307583013570L;

	private Home selectedHome;
	private boolean mainContentEditMode;

	public Home getSelectedHome() {
		return selectedHome;
	}

	public void setSelectedHome(Home selectedHome) {
		this.selectedHome = selectedHome;
	}

	public boolean isMainContentEditMode() {
		return mainContentEditMode;
	}

	public void setMainContentEditMode(boolean mainContentEditMode) {
		this.mainContentEditMode = mainContentEditMode;
	}

	public void initializeHome() {
		selectedHome = Home.getHomeForSelectedLanguage();
	}

	public void editMainContent() {
		mainContentEditMode = true;
	}

	public void save() {
		selectedHome = selectedHome.save();
		mainContentEditMode = false;
	}

	public void cancel() {
		mainContentEditMode = false;
	}

}
