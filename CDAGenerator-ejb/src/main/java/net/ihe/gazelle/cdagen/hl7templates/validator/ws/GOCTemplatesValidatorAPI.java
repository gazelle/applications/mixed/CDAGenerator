package net.ihe.gazelle.cdagen.hl7templates.validator.ws;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Local
@Path("/CDATemplatesValidator")
@Api("/CDATemplatesValidator")
public interface GOCTemplatesValidatorAPI {
	
	@GET
	@Path("/validateHL7TemplatesURL")
	@ApiOperation(value = "validate HL7Templates based on its URL from internet", 
					notes = "the result is a token to be used later", responseClass = "java.lang.String")
	@Produces("text/plain")
	public String validateHL7TemplatesURL(@ApiParam(value = "URL to the BBR", required = true) @QueryParam("decorURL") String decorURL, 
			@ApiParam(value = "username of the person validating", required = true) @QueryParam("username") String username, 
			@ApiParam(value = "unique identifier for the instance of art-decor validating. It can be the public ip address or the link to the art-decor instance", required = true) @QueryParam("artDecorInstanceIdentifier") String artDecorInstanceIdentifier);
	
	@GET
	@Path("/validateHL7Templates")
	@ApiOperation(value = "validate HL7Templates based on its content, an XML document transformed to a base64 string", 
		notes = "the result is a token to be used later", responseClass = "java.lang.String")
	@Produces("text/plain")
	public String validateHL7Templates(@ApiParam(value = "Base64 transformation of the BBR content", required = true) @QueryParam("decorXMLContentB64") String decorXMLContentB64, 
			@ApiParam(value = "username of the person validating", required = true) @QueryParam("username") String username, 
			@ApiParam(value = "unique identifier for the instance of art-decor validating. It can be the public ip address or the link to the art-decor instance", required = true) @QueryParam("artDecorInstanceIdentifier") String artDecorInstanceIdentifier) ;
	
	@GET
	@Path("/validateHL7Template")
	@ApiOperation(value = "validate a unique HL7Template based on its content, an XML part transformed to a base64 string", 
		notes = "not implemented yet", responseClass = "java.lang.String")
	@Produces("text/plain")
	public String validateHL7Template(@QueryParam("decorXMLContent") String decorXMLContentB64, 
			@QueryParam("username") String username, 
			@QueryParam("templateIdentifierId") String templateIdentifierId,
			@QueryParam("templateIdentifierName") String templateIdentifierName,
			@QueryParam("templateIdentifierVersionLabel") String templateIdentifierVersionLabel,
			@QueryParam("artDecorInstanceIdentifier") String artDecorInstanceIdentifier) ;
	
	@GET
	@Path("/validationStatus")
	@ApiOperation(value = "return the status of the validation, using a token", 
		notes="The status of validation is UNKNOWN, IN_PROGRESS, COMPLETED", responseClass = "java.lang.String")
	@Produces("text/plain")
	public ValidationStatus validationStatus(@ApiParam(value = "the token of the validation, a unique identifier", required = true) @QueryParam("validationToken") String validationToken);
	
	@GET
	@Path("/validationResult")
	@ApiOperation(value = "return the RESULT of the validation, using a token", responseClass = "java.lang.String")
	@Produces("text/xml")
	public String validationResult(@ApiParam(value = "the token of the validation, a unique identifier", required = true) @QueryParam("validationToken") String validationToken);
	
	@GET
	@Path("/validationLogs")
	@ApiOperation(value = "return the logs of the validation, using a token", responseClass = "java.lang.String")
	@Produces("text/plain")
	public String validationLogs(@ApiParam(value = "the token of the validation, a unique identifier", required = true) @QueryParam("validationToken") String validationToken);
	
	@GET
	@Path("/validationProgress")
	@ApiOperation(value = "return the progress of the validation, using a token", 
		notes = "The progress values are : 0%, 10%, 20%, 30%, 50%, 70%, 85%, 100%", responseClass = "java.lang.String")
	@Produces("text/plain")
	public String validationProgress(@ApiParam(value = "the token of the validation, a unique identifier", required = true) @QueryParam("validationToken") String validationToken);

}
