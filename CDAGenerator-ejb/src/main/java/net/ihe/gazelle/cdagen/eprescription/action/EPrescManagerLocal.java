package net.ihe.gazelle.cdagen.eprescription.action;

import javax.ejb.Local;

import org.richfaces.event.DropEvent;

import net.ihe.gazelle.cdagen.model.Organization;
import net.ihe.gazelle.cdagen.model.Patient;
import net.ihe.gazelle.cdagen.simulator.common.model.Concept;
import net.ihe.gazelle.patient.PatientGeneratorLocal;

@Local
public interface EPrescManagerLocal extends PatientGeneratorLocal {

    public abstract String getDdsDisplayMode();

    public abstract void setDdsDisplayMode(String ddsDisplayMode);

    public abstract Patient getSelectedAuthor();

    public abstract void setSelectedAuthor(Patient selectedAuthor);

    public abstract Organization getSelectedAuthorOrganization();

    public abstract void setSelectedAuthorOrganization(
            Organization selectedAuthorOrganization);

    public abstract Concept getSelectedAuthorRole();

    public abstract void setSelectedAuthorRole(Concept selectedAuthorRole);

    public abstract Patient getSelectedPatient();

    public abstract void setSelectedPatient(Patient selectedPatient);

    public abstract Organization getSelectedPatientOrganization();

    public abstract void setSelectedPatientOrganization(
            Organization selectedPatientOrganization);

    public abstract void processDropForAuthor(DropEvent dropevent);

    public abstract void processDropForPatient(DropEvent dropevent);
    
    public void destroy();
    
    public void generateAndSaveAuthor();
    
    public void generateAndSavePatient();

}