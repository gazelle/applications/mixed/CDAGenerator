package net.ihe.gazelle.cdagen.test.integration;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;

import org.junit.Test;

import net.ihe.gazelle.cdagen.validator.ws.DetailedResultTransformer;
import net.ihe.gazelle.validation.DetailedResult;

/**
 * Created by epoiseau on 08/02/2017.
 */
public class DetailedResultTransformerTest {
    @Test
    public void load() throws Exception {
        DetailedResult res = DetailedResultTransformer.load(new FileInputStream("src/test/resources/sampleValidationResult.xml"));
        assertTrue(res != null);
    }


}