package net.ihe.gazelle.cdagen.script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneratePagesFromMbvalDoc {

    private static final Logger LOG = LoggerFactory.getLogger(GeneratePagesFromMbvalDoc.class);

    static String pathToCdaEditorWar = "/home/gthomazon/workspace/cda-editor-war/src/main/webapp/editor/cda";
    static String pathToDatatypesEditorWar = "/home/gthomazon/workspace/datatypes-editor-war/src/main/webapp/editor/datatypes";

    public static void main2(String[] args) throws IOException {
        //POCDMT000040_CLINICAL_DOCUMENT_EDITOR("/editor/cda/POCDMT000040ClinicalDocument.xhtml", null, "CDA editor", Authorizations.ALL),
        String[] list = new File(pathToCdaEditorWar).list();
        List<String> res = new ArrayList();
        Map<String, String> pages = new HashMap<>();
        for (String fileName : list) {
            String label = updateCDAName(fileName.substring(0, fileName.indexOf(".")));

            String pageName = fileName.substring(0, fileName.indexOf(".")).toUpperCase();
            String fileLink = "/editor/cda/" + fileName;
            pages.put(fileName.substring(0, fileName.indexOf(".")).toUpperCase(), "/editor/cda/" + fileName);
            res.add(pageName + "(\"" + fileLink + "\",null,\"" + label + "\", Authorizations.ALL),");
        }

        for (String val : res) {
            System.out.println(val);
        }
    }

    public static void main(String[] args) throws IOException {
        //DATATYPES_CS_EDITOR("/editor/datatypes/CS.xhtml", null, "CS Editor", Authorizations.ALL),
        String[] list = new File(pathToDatatypesEditorWar).list();
        List<String> res = new ArrayList();
        Map<String, String> pages = new HashMap<>();
        for (String fileName : list) {
            String label = updateDatatypesName(fileName.substring(0, fileName.indexOf(".")));

            String pageName = "DATATYPES_" + fileName.substring(0, fileName.indexOf(".")).toUpperCase();
            String fileLink = "/editor/cda/" + fileName;
            pages.put(fileName.substring(0, fileName.indexOf(".")).toUpperCase(), "/editor/datatypes/" + fileName);
            res.add(pageName + "(\"" + fileLink + "\",null,\"" + label + "\", Authorizations.ALL),");
        }

        for (String val : res) {
            System.out.println(val);
        }
    }

    private static String updateCDAName(String s) {
        s = s.substring(12);
        s = s.replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
        return s;
    }

    private static String updateDatatypesName(String s) {
        s = s.replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
        return s;
    }

    public String getPathToCdaEditorWar() {
        return pathToCdaEditorWar;
    }

    public void setPathToCdaEditorWar(String pathToCdaEditorWar) {
        this.pathToCdaEditorWar = pathToCdaEditorWar;
    }

    public String getPathToDatatypesEditorWar() {
        return pathToDatatypesEditorWar;
    }

    public void setPathToDatatypesEditorWar(String pathToDatatypesEditorWar) {
        this.pathToDatatypesEditorWar = pathToDatatypesEditorWar;
    }

}
